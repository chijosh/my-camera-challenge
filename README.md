# MieterEngel coding challenge

Welcome to the MieterEngel coding challenge! Your mission, should
you accept it, is to build a page that our customers can use
to take a picture of a page with their mobile phone and send the picture
to us via email.
For the frontend part of this work please use React, every other decision
about which frameworks you use is up to you. The choice will only
matter for the evaluation insofar as e. g. adding buggy external
dependencies will also affect the stability of your code.

## The fronted
A layout idea for the frontend can be found [here](https://marvelapp.com/project/3366817/).

## The backend
The backend should send an email with the picture attached as a PDF to `coding-challenge@mieterengel.de`.

## What we will look for

We will check for the following when evaluating the coding challenge:
* __Functionality:__ The code should work and send out the email
* __Code organization:__ The code should be organized in files,
classes, functions, etc., based on the inherent structure of the
features
* __Code style:__ The code could should be easily readable
* __Git commit structure:__ Whether git commits are named and
structured meaningfully
* __Stability:__ The software should not break if mistreated by
customers
* __Security:__ The code should be secured against common attacks

There will be bonus points based on:
* __Improvement ideas:__ For a list of things you would propose to
add in the code if you had more time. This doesn't mean features, but
e. g. documentation and tests.
* __Documentation:__ If there is non-self-explanatory code, it
should be documented
* __Tests:__ If the code or parts that make sense are automatically
tested.
